import { AppDataSource } from "./data-source"
import { User } from "./entity/User"

AppDataSource.initialize().then(async () => {

    console.log("Inserting a new user into the database...")
    const useRepository = AppDataSource.getRepository(User)
    const user = new User()
    user.login = "Admin"
    user.name = "Admin"
    user.password = "Pass@1412"
    await useRepository.save(user)
    console.log("Saved a new user with id: " + user.id)

    console.log("Loading users from the database...")
    const users = await useRepository.find()
    console.log("Loaded users: ", users)

    

}).catch(error => console.log(error))
